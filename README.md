# rbow

This library is an experiment in integrating WebSockets with the Pyramid web framework.

An [ASGI](https://asgi.readthedocs.io/en/latest/) application is created to handle 
requests coming into the application. If the path has been registered as a websocket or 
ASGI view then the request is handled by rbow. 

Otherwise the request is handed off the the Pyramid WSGI application to process as
normally. The Pyramid application will have a new request property "rbow" that contains
functionality to interact with the ASGI aspects.

# WebSockets

Inherit from a consumer class to create a websocket endpoint.

```python
from rbow import websocket_config, AsyncWebsocketConsumer

@websocket_config(path="/ws-async")
class PingWebsocket(AsyncWebsocketConsumer):
    async def on_receive(self, data):
        await self.send("Pong!")
```

Consumer classes call `on_receive` with any received messages. Implement this method to
act on incoming messages. There is also an `on_receive_bytes` to handle incoming messages
that contain bytes instead of text.

Any unhandled exception in the receive methods will cause the websocket to be closed.

Override `on_connect` to act on new websocket connections (remember to call 
`super().on_connect()`). 

Send messages to the client with the `send` method.

There are a few different types of consumer base classes.

## AsyncWebsocketConsumer

All methods are implemented as asynchronous coroutines. 

```python
from rbow import websocket_config, AsyncWebsocketConsumer

@websocket_config(path="/my-websocket-path")
class PingWebsocket(AsyncWebsocketConsumer):
    async def on_receive(self, data):
        await self.send("Pong!")
```

## SyncWebsocketConsumer

All methods are implemented as normal methods. These are translated into
async calls by asgiref behind the scenes. 

```python
from rbow import websocket_config, SyncWebsocketConsumer

@websocket_config(path="/my-websocket-path")
class PingWebsocket(SyncWebsocketConsumer):
    def on_receive(self, data):
        self.send("Pong!")
```

## RequestConsumer

`on_receive` is a normal method that receives a Pyramid request. The return value will
be sent as a WebSocket message. A request transaction is started when on_receive is 
called and ends after it returns.

The value of the received message is in request.body.

```python
from rbow import websocket_config, RequestConsumer

@websocket_config(path="/my-websocket-path")
class PingWebsocket(RequestConsumer):
    def on_receive(self, request):
        return "Pong!"
```

# Channels

Channels allow broadcasting a message to multiple connected WebSockets.

```python
import json
from rbow import websocket_config, AsyncWebsocketConsumer


@websocket_config(path="/my-websocket-path")
class SubscribeChannel(AsyncWebsocketConsumer):

    async def on_receive(self, data):
        data = json.loads(data)
        channel_name = data['channelName']
        self.channels.subscribe(channel_name, self)
        # Pong! will be sent to all websockets that have connected to this endpoint
        # with the same channelName.
        self.channels.send('Pong!')

    async def on_client_close(self):
        await super().on_client_close()
        self.channels.unsubscribe_all(self)
```

An "interval channel" allows calling a function on a given interval and sending 
messages to all of the websockets connected to that channel periodically.

In this example `get_status` will be called every five seconds and send the current
date/time to all connected websockets.
```python
import json
from datetime import datetime
from rbow import websocket_config, AsyncWebsocketConsumer

@websocket_config(path="/my-websocket-path")
class SubscribePing(AsyncWebsocketConsumer):

    async def on_receive(self, data):
        data = json.loads(data)
        channel_name = data['channelName']
        self.channels.subscribe_to_interval(
            channel_name,
            self,
            interval=5, 
            fn=self.get_status,
        )

    async def on_client_close(self):
        await super().on_client_close()
        self.channels.unsubscribe_all(self)

    async def get_status(self, send):
        await send(datetime.now().isoformat())
```

Channels can also be sent messages from WSGI views.

```python
from cornice import Service
svc = Service(name="api ping", path="/api/ping")

@svc.post()
def ping(request):
    request.rbow['channels'].send('test-channel', 'Pong!')

```

# HTTP Views

ASGI can also be used for HTTP views.

There doesn't seem to be any reason to use this instead of normal Pyramid views.

```python
from rbow import asgi_view
from pyramid.response import Response


@asgi_view(path='/my-http-path')
async def demo(conn):
    request_body = await conn.receive()
    await conn.send(Response(
        'Pong!',
        content_type='text/html'
    ))
```

# Running

The application must be run using an ASGI server such as [Uvicorn](https://www.uvicorn.org/)
or [Daphne](https://github.com/django/daphne).

Uvicorn requires the application instance to be a global variable such as:

```python
from pyramid.config import Configurator

def main(global_config, **settings):
    with Configurator(settings=settings) as config:
        config.include("cornice")
        config.include("rbow")
        config.add_static_view("static", "static")
        config.add_route("web", "/web")
        config.scan("rbow_demo.views")
        return config.make_asgi_app()


application = main({})
``` 

And can be run with the command:

```
uvicorn --log-level debug --host 127.0.0.1 --port 6543 --interface asgi3 app_package:application
```

# TODO

* Make RequestConsumer have a real Pyramid request with all the fixins
* Include an option to parse all incoming/outgoing messages on a consumer from/to JSON
* Handle Pyramid responses returned from HTTP ASGI views completely
* Make a consistent API for extending consumers or scope with plugins (like getting a db connection)
* Make tests for everything
* Figure out what I was planning to do with the rbow.websocket.service module
* SocketIO Integration
