import logging
from asgiref.wsgi import WsgiToAsgi

from .context import build_app_context
from .interfaces import IAsgiViewUtility

log = logging.getLogger(__name__)


def make_asgi_app(config):
    """
    Creates an ASGI application the routes calls to ASGI views or
    Pyramid views. Normal Pyramid views are routed directly to the Pyramid WSGI
    app.
    """
    wsgi_app = config.make_wsgi_app()
    view_utility = config.registry.getUtility(IAsgiViewUtility)
    context = build_app_context(config)
    return ExtendedWsgiToAsgi(
        config, wsgi_app, view_utility.registrations, app_context=context)


class ExtendedWsgiToAsgi(WsgiToAsgi):
    """
    Wrap WSGI application into an ASGI application.

    Automatically uses ASGI for any ASGI views registered with add_asgi_view.
    """

    def __init__(self, config, wsgi_app, protocol_router, app_context):
        super().__init__(wsgi_app)
        self._config = config
        self._app_context = app_context
        self.protocol_router = protocol_router

    async def __call__(self, scope, receive, send):
        protocol = scope['type']
        path = scope['path']
        scope['rbow'] = self._app_context

        log.debug('Incoming %s request for %s', protocol, path)
        try:
            consumer = self.protocol_router[protocol][path]
        except KeyError:
            consumer = None
        if consumer is not None:
            log.debug('Routing request to consumer %s', consumer)
            return await consumer(scope, receive, send)
        log.debug('Routing request to WSGI application.')

        await super().__call__(scope, receive, send)

