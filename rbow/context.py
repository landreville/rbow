import asyncio

from rbow.channels.manager import ChannelManager


def build_app_context(config):
    """
    Return a dict of application-persistent data that rbow uses in it's
    request and scope extensions.
    """
    context = {
        "channels": ChannelManager()
    }
    config.add_request_method(
        lambda r: context, "rbow",
        property=True
    )
    return context
