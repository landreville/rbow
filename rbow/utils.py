import logging
from io import BytesIO
from pyramid.request import Request, apply_request_extensions
from pyramid.interfaces import IRequestExtensions
from pyramid.threadlocal import RequestContext as PyramidRequestContext

log = logging.getLogger(__name__)


def build_environ(scope, wsgi_input):
    """
    Return WSGI environ object built from given ASGI scope and optional message.

    If message is not given then `wsgi.input` will be empty
    """
    # TODO: Fix up this environ (check what asgiref does in WsgiToAsgi)
    environ = {
        'REQUEST_METHOD': scope.get('method'),
        'SCRIPT_NAME': '',
        'PATH_INFO': scope['path'],
        'QUERY_STRING': scope['query_string'].decode('ascii'),
        'SERVER_PROTOCOL': None,
        'wsgi.version': (1, 0),
        'wsgi.url_scheme': scope.get('scheme', 'ws'),
        'wsgi.input': BytesIO(wsgi_input),
        'wsgi.errors': BytesIO(),
        'wsgi.multithread': True,
        'wsgi.multiprocess': True,
        'wsgi.run_once': False,
        'webob.is_body_seekable': True,
        'CONTENT_LENGTH': str(len(wsgi_input)),
    }

    if 'server' in scope:
        environ['SERVER_NAME'] = scope['server'][0]
        environ['SERVER_PORT'] = scope['server'][1]
    else:
        environ['SERVER_NAME'] = 'localhost'
        environ['SERVER_PORT'] = 80
    
    for name, value in scope.get('headers', []):
        name = name.decode('latin1')
        if name == 'content-length':
            corrected_name = 'CONTENT_LENGTH'
        elif name == 'content-type':
            corrected_name = 'CONTENT_TYPE'
        else:
            corrected_name = 'HTTP_%s' % name.upper().replace('-', '_')

        value = value.decode('latin1')
        if corrected_name in environ:
            value = environ[corrected_name] + ',' + value
        environ[corrected_name] = value
    return environ


class RequestContext(PyramidRequestContext):

    def __init__(self, registry, scope, message):
        msg = message.get('text')
        if msg:
            data = msg.encode('utf-8')
        else:
            msg = message.get('bytes', b'')
            data = msg
        environ = build_environ(scope, data)
        path = scope['path']
        request = Request.blank(path, environ)
        request.body = data
        request.registry = registry
        extensions = registry.queryUtility(IRequestExtensions)
        if extensions is not None:
            apply_request_extensions(request, extensions=extensions)

        self.request = request
