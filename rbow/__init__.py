import asyncio
from typing import Dict

from .app import make_asgi_app
from .view import AsgiViewUtility, add_asgi_view, asgi_view, websocket_config
from .websocket import (
    AsyncWebsocketConsumer,
    SyncWebsocketConsumer,
    RequestConsumer
)

__all__ = [
    "asgi_view",
    "AsyncWebsocketConsumer",
    "RequestConsumer",
    "SyncWebsocketConsumer",
    "websocket_config",
]


def includeme(config):
    config.registry.registerUtility(AsgiViewUtility())
    config.add_directive("add_asgi_view", add_asgi_view)
    config.add_directive("make_asgi_app", make_asgi_app)




