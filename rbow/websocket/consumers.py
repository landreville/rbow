"""Classes that provide an interface for consuming WebSocket messages."""
import abc
import asyncio
import json
import logging

from asgiref.sync import async_to_sync, sync_to_async

from ..utils import RequestContext

log = logging.getLogger(__name__)


class JsonDunderEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, "__json__"):
            return obj.__json__()
        return super().default(obj)


class BaseConsumer(object, metaclass=abc.ABCMeta):
    """
    Dispatcher for WebSocket messages.
    """

    def __init__(self, registry, scope, send):
        self.registry = registry
        self.scope = scope
        self.raw_send = send
        self.closed = False
        self.channels = scope["rbow"]["channels"]

    @abc.abstractmethod
    def dispatch(self, message):
        """Dispatch the WebSocket message for handling."""
        pass


class AsyncWebsocketConsumer(BaseConsumer):
    """
    Consumes WebSocket messages.

    Sub-class and override on_receive and/or on_receive_bytes. These are automatically
    dispatched on WebSocket "receive" messages.

     All message handling methods are asynchronous.
    """

    async def accept(self):
        """Send a websocket.accept message to accept a new connection."""
        log.debug("Sending websocket.accept")
        await self.raw_send({"type": "websocket.accept"})

    async def close(self, code=None):
        """Send a websocket.close message to close the connection."""
        log.debug("Sending websocket.close")
        message = {"type": "websocket.close"}
        if code is not None:
            message["code"] = code
        await self.raw_send(message)

    async def dispatch(self, message):
        """Dispatch incoming WebSocket messages to on_connect, receive, or on_client_close."""
        log.debug("Dispatching message: %s", message)
        if message["type"] == "websocket.connect":
            await self.on_connect()
        elif message["type"] == "websocket.receive":
            try:
                await self.receive(message)
            except Exception:
                await self.close(1011)
                raise
        elif message["type"] in ("websocket.disconnect", "websocket.close"):
            await self.on_client_close()

    async def on_connect(self):
        """Called when a WebSocket connects and calls accept() to accept the connection."""
        await self.accept()

    async def on_client_close(self):
        """Sets self.closed to True to mark connection as closed."""
        self.closed = True

    async def on_receive(self, text_data):
        """
        Called when a websocket.receive message with text content is received.

        Override to implement code to handle websocket messages.
        """
        pass

    async def on_receive_bytes(self, bytes_data):
        """
        Called when a websocket.receive message with bytes content is received.

        Override to implement code to handle websocket messages.
        """
        pass

    async def receive(self, message):
        """
        Receives websocket.receive message dispatches to either on_receive or
        on_receive_bytes depending on the message type.
        """
        text_data = message.get("text")
        bytes_data = message.get("bytes")

        if text_data:
            await self.on_receive(text_data)
        if bytes_data:
            await self.on_receive_bytes(bytes_data)

        if not text_data and not bytes_data:
            log.warning(
                "Did not receive text or byte data on receive. Not dispatching."
            )

    async def send(self, value):
        """Send a WebSocket message."""
        log.debug("Sending data.")

        if self.closed:
            raise WebSocketClosedError()

        if isinstance(value, bytes):
            key = "bytes"
        else:
            key = "text"

        return await self.raw_send({"type": "websocket.send", key: value})

    async def send_json(self, value):
        """Stringify the value into a JSON message and send it."""
        return await self.send(json.dumps(value, cls=JsonDunderEncoder))


class SyncWebsocketConsumer(BaseConsumer):
    """
    Consumes WebSocket messages.

    Sub-class and override on_receive and/or on_receive_bytes. These are automatically
    dispatched on WebSocket "receive" messages.

     All message handling methods are synchronous. Sending messages is also synchronous
     and should not be awaited. 
    """

    def __init__(self, registry, scope, send):
        super().__init__(registry, scope, send)
        self.raw_send = async_to_sync(send)

    def accept(self):
        """Send a websocket.accept message to accept a new connection."""
        self.raw_send({"type": "websocket.accept"})

    def close(self, code=None):
        """Send a websocket.close message to close the connection."""
        message = {"type": "websocket.close"}
        if code is not None:
            message["code"] = code
        self.raw_send(message)

    @sync_to_async
    def dispatch(self, message):
        """Dispatch incoming WebSocket messages to on_connect, receive, or on_client_close."""
        if message["type"] == "websocket.connect":
            self.on_connect()
        elif message["type"] == "websocket.receive":
            try:
                self.receive(message)
            except Exception:
                self.close(1011)
                raise
        elif message["type"] in ("websocket.disconnect", "websocket.close"):
            self.on_client_close()

    def on_connect(self):
        """Called when a WebSocket connects and calls accept() to accept the connection."""
        self.accept()

    def on_client_close(self):
        """Sets self.closed to True to mark connection as closed."""
        self.closed = True

    def on_receive(self, text_data):
        """
        Called when a websocket.receive message with text content is received.

        Override to implement code to handle websocket messages.
        """
        pass

    def on_receive_bytes(self, bytes_data):
        """
        Called when a websocket.receive message with bytes content is received.

        Override to implement code to handle websocket messages.
        """
        pass

    def receive(self, message):
        """
        Receives websocket.receive message dispatches to either on_receive or
        on_receive_bytes depending on the message type.
        """
        text_data = message.get("text")
        bytes_data = message.get("bytes")

        if text_data:
            self.on_receive(text_data)

        if bytes_data:
            self.on_receive_bytes(bytes_data)

        if not text_data and not bytes_data:
            log.warning(
                "Did not receive text or byte data on receive. Not dispatching."
            )

    def send(self, value):
        """Send a WebSocket message."""
        if self.closed:
            raise WebSocketClosedError()

        if isinstance(value, bytes):
            key = "bytes"
        else:
            key = "text"

        return self.raw_send({"type": "websocket.send", key: value})

    def send_json(self, value):
        """Stringify the value into a JSON message and send it."""
        return self.send(json.dumps(value, cls=JsonDunderEncoder))


# TODO: figure out if this would be useful
#  The idea might have been to make on_receive transactional.
#  Each received message kicks off a new request transaction just like a
#  HTTP view-callable in Pyramid and ends the transaction when on_receive returns
class RequestConsumer(SyncWebsocketConsumer):
    """
    Synchronous consumer that provides a Pyramid request instance.

    On received messages a Pyramid request will be created and passed to on_receive.
    The message is put into request.body.
    """

    @abc.abstractmethod
    def on_receive(self, request):
        """
        Called when a websocket.receive message with text content is received.

        Override to implement code to handle websocket messages.
        Return a value to send a response message.
        """
        pass

    def receive(self, message):
        with RequestContext(self.registry, self.scope, message) as request:
            resp = self.on_receive(request)
            if resp is not None and not self.closed:
                self.send(resp)


class WebSocketClosedError(Exception):
    def __init__(self, msg=None):
        msg = msg if msg else "WebSocket is already closed."
        super().__init__(msg)
