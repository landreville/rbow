from .consumers import BaseConsumer


class AsgiWebsocket(object):
    """Wrap a WebSocket consumer class into an ASGI application."""

    def __init__(self, registry, consumer_class):
        self._consumer_class = consumer_class
        self._registry = registry

    async def __call__(self, scope, receive, send):
        """
        Bridge between ASGI and a WebSocket consumer.

        Waits for messages and dispatches them to the consumer class dispatch method.
        """
        stay_alive = True

        def disconnect():
            nonlocal stay_alive
            stay_alive = False

        consumer: BaseConsumer = self._consumer_class(
            self._registry, scope, SendWrapper(send, disconnect, disconnect)
        )

        while stay_alive:
            message = await receive()
            if message["type"] in ("websocket.disconnect", "websocket.close"):
                disconnect()
            await consumer.dispatch(message)


class SendWrapper(object):
    """
    Process send messages and disconnect the websocket on websocket.disconnect
    or websocket.close messages.
    """

    def __init__(self, send, on_disconnect, on_close):
        self._raw_send = send
        self._on_disconnect = on_disconnect
        self._on_close = on_close

    async def __call__(self, value):
        if value["type"] == "websocket.disconnect":
            self._on_disconnect()
        elif value["type"] == "websocket.close":
            if not value.get("code"):
                value["code"] = "1000"
            self._on_close()

        return await self._raw_send(value)
