from pyramid.response import Response


class AsgiConnection(object):

    def __init__(self, scope, receive, send):
        self.scope = scope
        self.send = send
        self.receive = receive


class AsgiHttpConnection(AsgiConnection):
    """
    Connection intended to receive a single HTTP request and send a single HTTP Response.

    Headers will be sent automatically the first time data is sent.
    """
    def __init__(self, scope, receive, send):
        self.scope = scope
        self._raw_send = send
        self._raw_receive = receive
        self._headers_sent = False

    async def receive(self):

        body = b''
        more_body = True

        while more_body:
            message = await self._raw_receive()
            body += message.get('body', b'')
            more_body = message.get('more_body', False)

        return body

    async def send(self, value):

        if isinstance(value, Response):
            # TODO: handle Pyramid Response more completely
            data = value.body
            headers = [(key.encode('ascii'), value.encode('ascii'))
                       for key, value in value.headerlist]
        else:
            data = value
            headers = []

        if not self._headers_sent:
            await self._raw_send({
                'type': 'http.response.start',
                'status': 200,
                'headers': headers
            })
            self._headers_sent = True

        if not isinstance(data, bytes):
            data = data.encode('utf-8')

        return await self._raw_send({
            'type': 'http.response.body',
            'body': data
        })

