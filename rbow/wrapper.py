from .connection import AsgiConnection, AsgiHttpConnection


class AsgiView(object):
    """
    Wrap a view-callable into an ASGI application.
    """

    connection_class = AsgiConnection

    def __init__(self, registry, view_callable):
        self._view_callable = view_callable

    async def __call__(self, scope, receive, send):
        return await self._view_callable(
            self.connection_class(scope, receive, send)
        )


class AsgiHttpView(AsgiView):
    """
    Wrap a view-callable into an ASGI application that uses AsgiHttpConnection
    to add HTTP stuff to the request and response.
    """

    connection_class = AsgiHttpConnection
