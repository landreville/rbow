import asyncio
import logging

from rbow.channels.channel import Channel, ChannelExists, IntervalChannel
from rbow.websocket.consumers import BaseConsumer

log = logging.getLogger(__name__)


class ChannelManager(object):
    def __init__(self, loop=None):
        self.loop = loop if loop else asyncio.get_event_loop()
        self.channels = {}

    def has_channel(self, channel_name):
        return channel_name in self.channels.keys()

    def subscribe(self, channel_name: str, consumer: BaseConsumer):
        """
        Subscribe to a channel.

        :param channel_name: Name of channel.
        :param consumer: WebSocket consumer.
        """

        self.channels.setdefault(channel_name, Channel(channel_name, self)).subscribe(
            consumer
        )

    def subscribe_to_interval(
        self, channel_name, consumer, interval, fn
    ) -> IntervalChannel:
        """
        Add an interval channel which will call the given function every period.

        :param request: Pyramid request.
        :param channel_name: Name of new channel.
        :param interval: Time period in seconds between each call to fn.
        :param fn: Function to call every interval.
        :return: IntervalChannel instance.
        """

        channel = self.channels.get(channel_name)
        if channel and (
            not isinstance(channel, IntervalChannel)
            or channel.on_interval != fn
            or channel.interval != interval
        ):
            raise ChannelExists(
                f'Channel "{channel_name}" exists and does not match given function or interval.'
            )
        else:
            channel = self.channels[channel_name] = IntervalChannel(
                channel_name, self, interval, fn
            )

        channel.subscribe(consumer)
        channel.start(self.loop)
        return channel

    def close_channel(self, channel_name):
        """Close and remove the channel."""
        try:
            self.channels[channel_name].close()
            del self.channels[channel_name]
        except KeyError:
            log.warning(
                f'Closing channel and could not find channel named "{channel_name}"'
            )

    def send(self, channel_name, message):
        """
        Send a message on a channel.
        """
        # This should work from both WSGI view-callables and ASGI views/websockets
        # however we can't wait to see if it succeeds with await.
        asyncio.run_coroutine_threadsafe(
            self.channels[channel_name].send(message), self.loop
        )

    def unsubscribe_all(self, consumer):
        """Unsubscribe the consumer from all channels."""

        for channel in self.channels.values():
            channel.unsubscribe(consumer)
