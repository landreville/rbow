"""Channel classes to manage sending messages to channels."""
import inspect
import logging
import abc
import asyncio
from typing import Any, Callable, Awaitable

from asgiref.sync import sync_to_async

from rbow.websocket.consumers import WebSocketClosedError

log = logging.getLogger(__name__)


class Channel(object):
    def __init__(self, name, manager):
        self.name = name
        self._manager = manager
        self._subscribers = set()

    def subscribe(self, consumer):
        """
        Subscribe the consumer to the channel.

        Consumers must have a send method that accepts the message and
        a closed method that returns True if they are not active.

        It's intended to use instances of AsyncWebsocketConsumer or
        SyncWebsocketConsumer as consumers.
        """
        self._subscribers.add(consumer)

    def unsubscribe(self, consumer):
        """Unsubscribe consumer from the channel."""
        self._subscribers.remove(consumer)

    async def send(self, message):
        """
        Send a message to all consumers of the channel.

        Closed websockets will be removed from the channel.
        """
        to_remove = set()
        send_tasks = []

        for consumer in self._subscribers:
            try:
                if inspect.iscoroutinefunction(consumer.send):
                    send_tasks.append(consumer.send(message))
                else:
                    # TODO: There's probably a better way to handle the
                    #  SyncWebsocketConsumer.send without the layers of
                    #  sync_to_async -> async_to_sync -> sync_to_async
                    send_tasks.append(sync_to_async(consumer.send)(message))
            except WebSocketClosedError:
                to_remove.add(consumer)
                continue

        self._subscribers = self._subscribers - to_remove

        await asyncio.gather(*send_tasks)

        if len(self._subscribers) == 0:
            log.debug(f'Closing channel "{self.name}" after last subscriber left.')
            self._manager.close_channel(self.name)

    def close(self):
        """Remove all subscribers from the channel."""
        self._subscribers.clear()

    def __del__(self):
        """Call close() in case there were any subscribers added late."""
        self.close()


class IntervalChannel(Channel, metaclass=abc.ABCMeta):
    def __init__(
        self,
        name: str,
        manager,
        interval: int,
        fn: Callable[[Callable[[Any], Awaitable]], Awaitable],
    ):
        super().__init__(name, manager)
        self.interval = interval
        self.on_interval = fn
        self._future = None
        self.running = False

    def start(self, loop):
        """
        Start calling the function every interval starting now.

        If the channel was already started this does nothing.
        """
        if not self.running:
            self._future = asyncio.run_coroutine_threadsafe(self._start_polling(), loop)

    async def _start_polling(self):
        """
        While there are subscribers, continue calling the function every interval.

        When there are zero subscribers the channel will stop running and start()
        must be called again.
        """
        self.running = True
        while len(self._subscribers) > 0:
            await self.on_interval(self.send)
            await asyncio.sleep(self.interval)

        self.running = False

    def close(self):
        """
        Close the channel.

        The currently running coroutine will be canceled if possible.
        """
        self.running = False
        self._future.cancel()
        super().close()


class ChannelExists(Exception):
    pass
