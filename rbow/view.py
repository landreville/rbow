import venusian
from zope.interface import implementer


from .interfaces import IAsgiViewUtility
from .wrapper import AsgiHttpView
from .websocket import AsgiWebsocket


_sentinel = object()


_default_view_wrappers = {
    'websocket': AsgiWebsocket,
    'http': AsgiHttpView,
}


def add_asgi_view(config, package, path, protocol='http', asgi_view_wrapper=_sentinel):
    """
    Add an ASGI view-callable.

    :param config: Pyramid configurator. This is automatically present when calling
        config.add_asgi_view.
    :param package: A Python callable or dotted Python name which refers to a callable.
    :param path: Path to serve this view-callable on.
    :param protocol: websocket or http.
    """
    view_callable = config.maybe_dotted(package)

    if asgi_view_wrapper is _sentinel:
        asgi_view_wrapper = _default_view_wrappers[protocol]

    if asgi_view_wrapper is not None:
        view_callable = asgi_view_wrapper(config.registry, view_callable)

    config.registry.getUtility(IAsgiViewUtility).register(
        path,
        protocol,
        view_callable
    )


class asgi_view(object):
    """
    Decorator for add_asgi_view that accepts HTTP requests.

    Takes the same arguments as add_asgi_view.
    """
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def _register(self, scanner, name, wrapped):
        scanner.config.add_asgi_view(wrapped, *self.args, **self.kwargs)

    def __call__(self, wrapped):
        venusian.attach(wrapped, self._register)
        return wrapped


class websocket_config(object):
    """
    Decorator for add_asgi_view that accepts WebSocket connections.

    Takes the same arguments as add_asgi_view, protocol is set to 'websocket'.
    """

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def _register(self, scanner, name, wrapped):
        scanner.config.add_asgi_view(
            wrapped,
            *self.args,
            protocol='websocket',
            asgi_view_wrapper=AsgiWebsocket,
            **self.kwargs
        )

    def __call__(self, wrapped):
        venusian.attach(wrapped, self._register)
        return wrapped


@implementer(IAsgiViewUtility)
class AsgiViewUtility(object):
    """Registry utility the keeps track of ASGI views that have been registered."""

    def __init__(self):
        self.registrations = {'http': {}, 'websocket': {}}

    def register(self, path, protocol, fn):
        self.registrations[protocol][path] = fn
